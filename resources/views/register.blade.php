<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 1 - Form</title>
</head>

<body>
    <!-- Membuat Form -->
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="FirstName"><b>First Name:</b></label><br><br>
        <input type="text" id="FirstName" class="first" name="first" placeholder="First Name" required><br><br>
        <label for="LastName"><b>Last Name:</b></label><br><br>
        <input type="text" id="LastName" class="last" name="last" placeholder="Last Name" required><br><br>
        <label><b>Gender :</b></label><br><br>
        <input type="radio" id="male" name="genders" checked> <label for="male">Male</label><br>
        <input type="radio" id="female" name="genders"> <label for="female">Female</label><br>
        <input type="radio" id="other" name="genders"> <label for="other">Other</label><br><br>
        <label for="national"><b> Nationaly:</b></label><br><br>
        <select name="nationaly" id="negara"> 
            
            <option><label for="negara">Indonesian</label></option>
            <option><label for="negara">English</label></option>
            <option><label for="negara">Japanese</label></option>
            <option><label for="negara">Rusian</label></option>

        </select><br><br>
        <b><label for="language">Language Spoken:</label></b><br><br>
        <input type="checkbox" name="Languages" id="language" checked><label for="language">Bahasa Indonesia</label>
        <br>
        <input type="checkbox" name="Languages" id="bahasa"><label for="bahasa">English</label> <br>
        <input type="checkbox" name="Languages" id="bahasa2"><label for="bahasa2">Other</label> <br><br>
        <label for=""><b>Bio:</b></label><br><br>
        <textarea name="Bio" cols="30" rows="12" placeholder="Bio"></textarea><br>
        <input type="submit" value="Submit">

    </form>
</body>

</html>